import os
from dotenv import load_dotenv
import re
import random 
import pickle
from discord.ext import commands
import pprint 


load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')
GUILD = os.getenv('DISCORD_GUILD')
SASS_FILE = '/home/neko3/slackbot/' + 'sass.pickle'
PATTERNS = {}
SASS = {}

control_roles = ['root', 'otter-queen']
bot = commands.Bot('?')
pp = pprint.PrettyPrinter(indent=2)

def load_file():
    try:
        SASS = pickle.load(open(SASS_FILE, 'rb'))
    except FileNotFoundError:
        print("No SASS file to load", SASS_FILE)
        return
    return SASS


def create_regex_patterns():
    if len(SASS.keys()) == 0:
        print("No patterns to create")
        return
    for k, v in SASS.items():
        p = re.compile(r'\b' + r'{}'.format(k) + r'\b', re.IGNORECASE)
        PATTERNS[k] = p
    return PATTERNS

#pickle.dump(SASS, open(SASS_FILE, 'wb'), protocol=0)

@bot.event
async def on_ready():
    for guild in bot.guilds:
        if guild.name == GUILD:
            break

    print(
        f'{bot.user} is connected to the following guild:\n'
        f'{guild.name}(id: {guild.id})\n'
    )



@bot.event
async def on_message(message):
    if message.author == bot.user:
        return

    for k, v in PATTERNS.items():
        m = v.search(message.content)
        if not m is None:
            #print("trigger", k)
            reply = random.choice(SASS[k])
            await message.channel.send(reply)
            break
    await bot.process_commands(message)

@bot.event
async def on_error(event, *args, **kwargs):
    with open('err.log', 'a') as f:
        if event == 'on_message':
            f.write(f'Unhandled message: {args[0]}\n')
        else:
            raise

@bot.command(name='add', help='Register a new keyword -- response pair. Wrap response in "", keyword too if it contains spaces.')
@commands.has_any_role(*control_roles)
async def set_keyword(ctx, keyword: str, response: str):
    
    ## check if keyword in dictionary
    if not keyword in SASS.keys():
        ## add new key to dict
        SASS[keyword] = []
        ## create pattern
        p = re.compile(r'\b' + r'{}'.format(keyword) + r'\b', re.IGNORECASE)
        PATTERNS[keyword] = p
    ## add new response
    SASS[keyword].append(response)
    ## save to pickle file
    pickle.dump(SASS, open(SASS_FILE, 'wb'), protocol=0)
    
    ## confirm
    await ctx.send('New trigger added on keyword: {}\nand response: {}'.format(keyword, response))
#    return (SASS, PATTERNS)


@bot.command(name='list')
async def list_dictionary(ctx):
    response = pp.pformat(SASS)
    await ctx.send('```'+response+'```')

@bot.command(name='deleteK', help='Deletes all replies of a keyword and removes the trigger')
@commands.has_any_role(*control_roles)
async def delete_keyword(ctx, keyword):
    print('keyword: {}'.format(keyword))
    if keyword not in SASS.keys():
        await ctx.send('Keyword {} not in dictionary. Soz'.format(keyword))
        return
    SASS.pop(keyword, None)
    ## update pickle file
    pickle.dump(SASS, open(SASS_FILE, 'wb'), protocol=0)
    ## remove from patterns
    PATTERNS.pop(keyword, None)
    await ctx.send('Deleted keyword {} and ALL associated responses'.format(keyword))

@bot.command(name='deleteR', help='Deletes a reply given a keyword and a string which will be matched against the replies list; ALL matching replies will be deleted')
@commands.has_any_role(*control_roles)
async def delete_reply(ctx, keyword: str, reply: str):
    print('keyword: {}\nreply:{}'.format(keyword, reply))
    if keyword not in SASS.keys():
        await ctx.send('Keyword {} not in dictionary. Soz'.format(keyword))
        return
    to_remove = []
    for _r in SASS[keyword]:
        if _r.find(reply) != -1:
            to_remove.append(_r)

    print(to_remove) 
    for _r in to_remove:
        SASS[keyword].remove(_r)
    ## check if there are any replies left
    if len(SASS[keyword]) == 0:
        ## if not, delete keyword
        SASS.pop(keyword)
        PATTERNS.pop(keyword)

    ## update pickle file
    pickle.dump(SASS, open(SASS_FILE, 'wb'), protocol=0)
    await ctx.send('Deleted associated response(s)for keyword {} {}'.format(keyword, ', '.join(to_remove)))
    

@bot.event
async def on_command_error(ctx, error):
    if isinstance(error, commands.errors.CheckFailure):
        await ctx.send('You do not have the correct role for this command.')

if __name__ == '__main__':
    SASS = load_file()
    PATTERNS = create_regex_patterns()
    bot.run(TOKEN)
